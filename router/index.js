const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");

router.get("/examen", (req, res)=>{
    const valores = {
        numRecibo:req.query.numRecibo,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio,
        tipoServicio:req.query.tipoServicio,
        kConsumidos:req.query.kConsumidos
    }
    res.render('examen.html', valores);
})

router.post("/examen", (req, res)=>{
    const valores = {
        numRecibo:req.body.numRecibo,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        tipoServicio:req.body.tipoServicio,
        kConsumidos:req.body.kConsumidos
    }
    res.render('examen.html', valores);
})

module.exports = router; 